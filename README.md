Unserer System besteht aus Frontend, Prismic, Crawler, API und einer Datenbank.


Um **Prismic** zu nutzen benötigt man lediglich den Prismic Zugang:

~~~~
_https://prismic.io/dashboard_

_Benutzer: awmt@web.de_

_Passwort: awmt2021_
~~~~

Die **API** befindet sich in unserem backend Projekt: https://gitlab.com/awmt/backend.
Hierzu bitte die dort vorliegende Readme nutzen.

Der **Crawler** hat ebenfalls ein eigenes Projekt: https://gitlab.com/awmt/crawler.
Auch hier die Crawler interne Readme beachten.

Das **Frontend** ist hier zu finden: https://gitlab.com/awmt/zukunft-sichern.
Hier haben wir ebenfalls im Projekt eine Readme vorbereitet.


Nachdem alle Projekte zum Deploy vorbereitet wurden, muss die docker-compose.yml aus diesem Projekt genutzt werden um alle Dienste gemeinsam zu starten.
Bevor wir mit `docker-compose up -d` alle Container starten sollten wir zum einen die ports überprüfen, die in der docker-compose.yml angegeben sind. Zudem müssen über den `docker volume create` Befehl, die zwei Volumes (api-files, awmt-database) erzeugen.
